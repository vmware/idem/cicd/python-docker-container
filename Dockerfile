ARG PYTHON_VERSION

FROM python:${PYTHON_VERSION}

RUN apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y \
    && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
    && echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker-ce.list \
    && apt update && apt install -y docker-ce-cli
